// This file is part of CPAchecker,
// a tool for configurable software verification:
// https://cpachecker.sosy-lab.org
//
// SPDX-FileCopyrightText: 2007-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.cfa.postprocessing.function;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import org.sosy_lab.common.configuration.Configuration;
import org.sosy_lab.common.configuration.InvalidConfigurationException;
import org.sosy_lab.common.configuration.Option;
import org.sosy_lab.common.configuration.Options;
import org.sosy_lab.common.log.LogManager;
import org.sosy_lab.cpachecker.cfa.CFA;
import org.sosy_lab.cpachecker.cfa.CFACreationUtils;
import org.sosy_lab.cpachecker.cfa.MutableCFA;
import org.sosy_lab.cpachecker.cfa.ast.FileLocation;
import org.sosy_lab.cpachecker.cfa.ast.c.CArraySubscriptExpression;
import org.sosy_lab.cpachecker.cfa.ast.c.CAssignment;
import org.sosy_lab.cpachecker.cfa.ast.c.CBinaryExpression.BinaryOperator;
import org.sosy_lab.cpachecker.cfa.ast.c.CBinaryExpressionBuilder;
import org.sosy_lab.cpachecker.cfa.ast.c.CCastExpression;
import org.sosy_lab.cpachecker.cfa.ast.c.CExpression;
import org.sosy_lab.cpachecker.cfa.ast.c.CExpressionAssignmentStatement;
import org.sosy_lab.cpachecker.cfa.ast.c.CFieldReference;
import org.sosy_lab.cpachecker.cfa.ast.c.CFunctionCallAssignmentStatement;
import org.sosy_lab.cpachecker.cfa.ast.c.CFunctionCallExpression;
import org.sosy_lab.cpachecker.cfa.ast.c.CFunctionCallStatement;
import org.sosy_lab.cpachecker.cfa.ast.c.CFunctionDeclaration;
import org.sosy_lab.cpachecker.cfa.ast.c.CIdExpression;
import org.sosy_lab.cpachecker.cfa.ast.c.CIntegerLiteralExpression;
import org.sosy_lab.cpachecker.cfa.ast.c.CLeftHandSide;
import org.sosy_lab.cpachecker.cfa.ast.c.CPointerExpression;
import org.sosy_lab.cpachecker.cfa.ast.c.CRightHandSide;
import org.sosy_lab.cpachecker.cfa.ast.c.CSimpleDeclaration;
import org.sosy_lab.cpachecker.cfa.ast.c.CStatement;
import org.sosy_lab.cpachecker.cfa.ast.c.CThreadOperationStatement.CThreadCreateStatement;
import org.sosy_lab.cpachecker.cfa.ast.c.CThreadOperationStatement.CThreadJoinStatement;
import org.sosy_lab.cpachecker.cfa.ast.c.CUnaryExpression;
import org.sosy_lab.cpachecker.cfa.ast.c.CVariableDeclaration;
import org.sosy_lab.cpachecker.cfa.model.CFAEdge;
import org.sosy_lab.cpachecker.cfa.model.CFANode;
import org.sosy_lab.cpachecker.cfa.model.FunctionEntryNode;
import org.sosy_lab.cpachecker.cfa.model.c.CAssumeEdge;
import org.sosy_lab.cpachecker.cfa.model.c.CFunctionEntryNode;
import org.sosy_lab.cpachecker.cfa.model.c.CStatementEdge;
import org.sosy_lab.cpachecker.cfa.types.c.CStorageClass;
import org.sosy_lab.cpachecker.cfa.types.c.CType;
import org.sosy_lab.cpachecker.exceptions.UnrecognizedCodeException;
import org.sosy_lab.cpachecker.util.CFATraversal;
import org.sosy_lab.cpachecker.util.CFATraversal.TraversalProcess;

@Options
public class ThreadCreateTransformer {

  @Option(
      secure = true,
      name = "cfa.threads.threadCreate",
      description = "A name of thread_create function")
  private Set<String> threadCreate = ImmutableSet.of("pthread_create");

  @Option(
      secure = true,
      name = "cfa.threads.threadSelfCreate",
      description = "A name of thread_create_N function")
  private String threadCreateN = "pthread_create_N";

  @Option(
      secure = true,
      name = "cfa.threads.threadJoin",
      description = "A name of thread_join function")
  private String threadJoin = "pthread_join";

  @Option(
      secure = true,
      name = "cfa.threads.threadSelfJoin",
      description = "A name of thread_join_N function")
  private String threadJoinN = "pthread_join_N";

  @Option(
      secure = true,
      name = "cfa.threads.addAssumptions",
      description = "Add assumptions about successfull thread create")
  private boolean addAssumptions = true;

  private final Map<String, ThreadCreateDesc> description;

  @Option(
      secure = true,
      name = "cfa.threads.preset",
      description = "Preset values for some threads")
  private Set<String> presetThreads = ImmutableSet.of("");

  private final Map<String, Integer> presetThreadInfo;

  public boolean isThreadOperation(CFunctionCallExpression pCallExpr) {
    String fName = pCallExpr.getFunctionNameExpression().toString();
    return threadCreate.contains(fName)
        || fName.equals(threadCreateN)
        || fName.equals(threadJoin)
        || fName.equals(threadJoinN);
  }

  private static class ThreadCreateDesc {
    public final int entry;
    public final int variable;
    public final int priority;
    public final int arguments;

    public ThreadCreateDesc(int pEntry, int pVar, int pArgs, int pPriority) {
      entry = pEntry;
      variable = pVar;
      priority = pPriority;
      arguments = pArgs;
    }
  }

  public class ThreadFinder implements CFATraversal.CFAVisitor {

    private final Map<CFAEdge, CFunctionCallExpression> threadCreates = new HashMap<>();
    private final Map<CFAEdge, CFunctionCallExpression> threadJoins = new HashMap<>();

    public Map<CFAEdge, CFunctionCallExpression> getThreadCreates() {
      return threadCreates;
    }

    private Map<CFAEdge, CFunctionCallExpression> getThreadJoins() {
      return threadJoins;
    }

    @Override
    public TraversalProcess visitEdge(CFAEdge pEdge) {
      if (pEdge instanceof CStatementEdge) {
        CStatement statement = ((CStatementEdge) pEdge).getStatement();
        if (statement instanceof CAssignment) {
          CRightHandSide rhs = ((CAssignment) statement).getRightHandSide();
          if (rhs instanceof CFunctionCallExpression exp) {
            checkFunctionExpression(pEdge, exp);
          }
        } else if (statement instanceof CFunctionCallStatement) {
          CFunctionCallExpression exp =
              ((CFunctionCallStatement) statement).getFunctionCallExpression();
          checkFunctionExpression(pEdge, exp);
        }
      }
      return TraversalProcess.CONTINUE;
    }

    @Override
    public TraversalProcess visitNode(CFANode pNode) {
      return TraversalProcess.CONTINUE;
    }

    private void checkFunctionExpression(CFAEdge edge, CFunctionCallExpression exp) {
      String fName = exp.getFunctionNameExpression().toString();
      if (threadCreate.contains(fName) || fName.equals(threadCreateN)) {
        threadCreates.put(edge, exp);
      } else if (fName.equals(threadJoin) || fName.equals(threadJoinN)) {
        threadJoins.put(edge, exp);
      }
    }
  }

  private final LogManager logger;

  public ThreadCreateTransformer(LogManager log, Configuration config)
      throws InvalidConfigurationException {
    config.inject(this);
    description = new TreeMap<>();
    presetThreadInfo = new TreeMap<>();
    logger = log;
    FillDescription(config);
  }

  private void FillDescription(Configuration config) {
    for (String fname : threadCreate) {
      int entryNum = getOrDefault(config, fname + ".entry", 2);
      int varNum = getOrDefault(config, fname + ".variable", 0);
      int argNum = getOrDefault(config, fname + ".arguments", 3);
      int priorityNum = getOrDefault(config, fname + ".priority", -1);
      description.put(fname, new ThreadCreateDesc(entryNum, varNum, argNum, priorityNum));
    }
    int entryNum = getOrDefault(config, threadCreateN + ".entry", 2);
    int varNum = getOrDefault(config, threadCreateN + ".variable", 0);
    int argNum = getOrDefault(config, threadCreateN + ".arguments", 3);
    int priorityNum = getOrDefault(config, threadCreateN + ".priority", -1);
    description.put(threadCreateN, new ThreadCreateDesc(entryNum, varNum, argNum, priorityNum));

    for (String fName : presetThreads) {
      priorityNum = getOrDefault(config, fName + ".presetPriority", -1);
      if (priorityNum >= 0) {
        presetThreadInfo.put(fName, priorityNum);
      }
    }
  }

  @SuppressWarnings("deprecation")
  private static int getOrDefault(Configuration config, String property, int pDefault) {
    try {
      String line = config.getProperty(property);
      if (line == null) {
        return pDefault;
      }
      return Integer.parseInt(line);
    } catch (NumberFormatException e) {
      return pDefault;
    }
  }

  public void transform(CFA cfa) {
    ThreadFinder threadVisitor = new ThreadFinder();

    for (FunctionEntryNode functionStartNode : cfa.entryNodes()) {
      CFATraversal.dfs().traverseOnce(functionStartNode, threadVisitor);
    }

    // We need to repeat this loop several times, because we traverse that part cfa, which is
    // reachable from main
    for (Entry<CFAEdge, CFunctionCallExpression> entry :
        threadVisitor.getThreadCreates().entrySet()) {
      CFAEdge edge = entry.getKey();
      CFunctionCallExpression fCall = entry.getValue();

      String fName = fCall.getFunctionNameExpression().toString();
      List<CExpression> args = fCall.getParameterExpressions();
      ThreadCreateDesc tDesc = description.get(fName);

      CExpression calledFunction = args.get(tDesc.entry);
      CIdExpression functionNameExpression = getFunctionName(calledFunction);
      if (functionNameExpression == null) {
        logger.log(
            Level.WARNING, "Unsupported function name: " + calledFunction + ", full line: " + edge);
        continue;
      }
      String newThreadName = functionNameExpression.getName();
      CFunctionEntryNode entryNode = (CFunctionEntryNode) cfa.getFunctionHead(newThreadName);
      if (entryNode == null) {
        logger.log(
            Level.WARNING,
            "Can not find the body of function " + newThreadName + "(), full line: " + edge);
        continue;
      }
      String varName;
      if (tDesc.variable >= 0) {
        varName = getThreadVariableName(fCall, tDesc.variable);
      } else {
        // Assignment of return value
        CStatement stmnt = ((CStatementEdge) edge).getStatement();
        CLeftHandSide leftSide = ((CAssignment) stmnt).getLeftHandSide();
        varName = getThreadVariableName(leftSide);
      }
      List<CExpression> functionParameters;
      if (tDesc.arguments > 0) {
        functionParameters = Lists.newArrayList(args.get(tDesc.arguments));
      } else {
        functionParameters = new ArrayList<>();
      }

      CFunctionDeclaration functionDeclaration = entryNode.getFunctionDefinition();
      FileLocation pFileLocation = edge.getFileLocation();

      CFunctionCallExpression pFunctionCallExpression =
          new CFunctionCallExpression(
              pFileLocation,
              functionDeclaration.getType().getReturnType(),
              functionNameExpression,
              functionParameters,
              functionDeclaration);

      boolean isSelfParallel = fName.equals(threadCreateN);
      int priority = -1; // Unknown
      if (presetThreadInfo.containsKey(newThreadName)) {
        priority = presetThreadInfo.get(newThreadName);
      } else if (tDesc.priority >= 0) {
        // Expecting priority
        CExpression prioExp = args.get(tDesc.priority);
        if (prioExp instanceof CCastExpression) {
          prioExp = ((CCastExpression) prioExp).getOperand();
        }
        if (prioExp instanceof CIntegerLiteralExpression) {
          priority = ((CIntegerLiteralExpression) prioExp).getValue().intValue();
        }
      }
      CFunctionCallStatement pFunctionCall =
          new CThreadCreateStatement(
              pFileLocation, pFunctionCallExpression, isSelfParallel, varName, priority);

      if (edge instanceof CStatementEdge) {
        CStatement stmnt = ((CStatementEdge) edge).getStatement();
        if (stmnt instanceof CFunctionCallAssignmentStatement) {
          String pRawStatement = edge.getRawStatement();

          CFANode pPredecessor = edge.getPredecessor();
          CFANode pSuccessor = edge.getSuccessor();

          CFACreationUtils.removeEdgeFromNodes(edge);

          if (addAssumptions) {
            // We should replace r = pthread_create(f) into
            // - r = TMP;
            // - [r == 0]
            // - f()
            CFANode firstNode = new CFANode(pPredecessor.getFunction());
            CFANode secondNode = new CFANode(pPredecessor.getFunction());
            ((MutableCFA) cfa).addNode(firstNode);
            ((MutableCFA) cfa).addNode(secondNode);

            CStatement assign = prepareRandomAssignment((CFunctionCallAssignmentStatement) stmnt);
            CStatementEdge randAssign =
                new CStatementEdge(pRawStatement, assign, pFileLocation, pPredecessor, firstNode);

            CExpression assumption =
                prepareAssumption((CFunctionCallAssignmentStatement) stmnt, cfa);
            CAssumeEdge trueEdge =
                new CAssumeEdge(
                    pRawStatement, pFileLocation, firstNode, secondNode, assumption, true);
            CAssumeEdge falseEdge =
                new CAssumeEdge(
                    pRawStatement, pFileLocation, firstNode, pSuccessor, assumption, false);

            CFACreationUtils.addEdgeUnconditionallyToCFA(randAssign);
            CFACreationUtils.addEdgeUnconditionallyToCFA(trueEdge);
            CFACreationUtils.addEdgeUnconditionallyToCFA(falseEdge);
            pPredecessor = secondNode;
          }

          CStatementEdge callEdge =
              new CStatementEdge(
                  pRawStatement, pFunctionCall, pFileLocation, pPredecessor, pSuccessor);

          CFACreationUtils.addEdgeUnconditionallyToCFA(callEdge);

          logger.log(Level.FINE, "Replace " + edge + " with " + callEdge);
        } else {
          replaceEdgeWith(edge, pFunctionCall);
        }

      } else {
        replaceEdgeWith(edge, pFunctionCall);
      }
    }

    for (Entry<CFAEdge, CFunctionCallExpression> entry :
        threadVisitor.getThreadJoins().entrySet()) {
      CFAEdge edge = entry.getKey();
      CFunctionCallExpression fCall = entry.getValue();
      String varName = getThreadVariableName(fCall, 0);
      FileLocation pFileLocation = edge.getFileLocation();

      String fName = fCall.getFunctionNameExpression().toString();
      boolean isSelfParallel = !fName.equals(threadJoin);
      CFunctionCallStatement pFunctionCall =
          new CThreadJoinStatement(pFileLocation, fCall, isSelfParallel, varName);

      replaceEdgeWith(edge, pFunctionCall);
    }
  }

  private void replaceEdgeWith(CFAEdge edge, CFunctionCallStatement fCall) {
    CFANode pPredecessor = edge.getPredecessor();
    CFANode pSuccessor = edge.getSuccessor();
    FileLocation pFileLocation = edge.getFileLocation();
    String pRawStatement = edge.getRawStatement();

    CFACreationUtils.removeEdgeFromNodes(edge);

    CStatementEdge callEdge =
        new CStatementEdge(pRawStatement, fCall, pFileLocation, pPredecessor, pSuccessor);

    logger.log(Level.FINE, "Replace " + edge + " with " + callEdge);
    CFACreationUtils.addEdgeUnconditionallyToCFA(callEdge);
  }

  private int tmpVarCounter = 0;

  private CStatement prepareRandomAssignment(CFunctionCallAssignmentStatement stmnt) {
    FileLocation pFileLocation = stmnt.getFileLocation();
    CFunctionCallExpression fCall = stmnt.getFunctionCallExpression();
    CLeftHandSide left = stmnt.getLeftHandSide();

    String tmpName = "CPA_TMP_" + tmpVarCounter++;
    CType retType = fCall.getDeclaration().getType().getReturnType();
    CSimpleDeclaration decl =
        new CVariableDeclaration(
            pFileLocation, false, CStorageClass.AUTO, retType, tmpName, tmpName, tmpName, null);
    CIdExpression tmp = new CIdExpression(pFileLocation, decl);

    return new CExpressionAssignmentStatement(pFileLocation, left, tmp);
  }

  private CExpression prepareAssumption(CFunctionCallAssignmentStatement stmnt, CFA cfa) {
    CLeftHandSide left = stmnt.getLeftHandSide();

    CBinaryExpressionBuilder bBuilder = new CBinaryExpressionBuilder(cfa.getMachineModel(), logger);

    try {
      return bBuilder.buildBinaryExpression(
          left, CIntegerLiteralExpression.ZERO, BinaryOperator.EQUALS);
    } catch (UnrecognizedCodeException e) {
      throw new UnsupportedOperationException("Cannot proceed: ", e);
    }
  }

  public static CIdExpression getFunctionName(CExpression fName) {
    if (fName instanceof CIdExpression) {
      return (CIdExpression) fName;
    } else if (fName instanceof CUnaryExpression) {
      return getFunctionName(((CUnaryExpression) fName).getOperand());
    } else if (fName instanceof CCastExpression) {
      return getFunctionName(((CCastExpression) fName).getOperand());
    } else {
      return null;
    }
  }

  private String getThreadVariableName(CFunctionCallExpression fCall, int pos) {
    return getThreadVariableName(fCall.getParameterExpressions().get(pos));
  }

  private String getThreadVariableName(CExpression pVar) {
    CExpression var = pVar;

    while (true) {
      if (var instanceof CIdExpression) {
        return ((CIdExpression) var).getName();
      } else if (var instanceof CFieldReference) {
        return getThreadVariableName(((CFieldReference) var).getFieldOwner())
            + "."
            + ((CFieldReference) var).getFieldName();
      } else if (var instanceof CUnaryExpression) {
        // &t
        var = ((CUnaryExpression) var).getOperand();
      } else if (var instanceof CCastExpression) {
        // (void *(*)(void * ))(& ldv_factory_scenario_4)
        var = ((CCastExpression) var).getOperand();
      } else if (var instanceof CArraySubscriptExpression) {
        var = ((CArraySubscriptExpression) var).getArrayExpression();
      } else if (var instanceof CPointerExpression) {
        var = ((CPointerExpression) var).getOperand();
      } else {
        throw new UnsupportedOperationException("Unsupported parameter expression " + var);
      }
    }
  }
}
