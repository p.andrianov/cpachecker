// This file is part of CPAchecker,
// a tool for configurable software verification:
// https://cpachecker.sosy-lab.org
//
// SPDX-FileCopyrightText: 2007-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.cpa.usage;

import org.sosy_lab.cpachecker.cfa.blocks.Block;
import org.sosy_lab.cpachecker.cfa.model.CFANode;
import org.sosy_lab.cpachecker.cfa.model.FunctionExitNode;
import org.sosy_lab.cpachecker.core.interfaces.AbstractState;
import org.sosy_lab.cpachecker.core.interfaces.Precision;
import org.sosy_lab.cpachecker.core.interfaces.Reducer;

public class UsageReducer implements Reducer {
  private final Reducer wrappedReducer;

  public UsageReducer(Reducer pWrappedReducer) {
    wrappedReducer = pWrappedReducer;
  }

  @Override
  public AbstractState getVariableReducedState(
      AbstractState pExpandedElement, Block pContext, CFANode pLocation)
      throws InterruptedException {

    UsageState funElement = (UsageState) pExpandedElement;
    AbstractState red =
        wrappedReducer.getVariableReducedState(funElement.getWrappedState(), pContext, pLocation);
    // return funElement.reduced(red, pLocation.getFunction().getName());
    return funElement.copy(red);
  }

  @Override
  public AbstractState getVariableExpandedState(
      AbstractState pRootElement, Block pReducedContext, AbstractState pReducedElement)
      throws InterruptedException {
    UsageState funRootState = (UsageState) pRootElement;
    UsageState funReducedState = (UsageState) pReducedElement;
    AbstractState exp =
        wrappedReducer.getVariableExpandedState(
            funRootState.getWrappedState(), pReducedContext, funReducedState.getWrappedState());

    // return funRootState
    // .expanded(exp, funReducedState, pReducedContext.getCallNode().getFunction().getName());
    return funRootState.copy(exp);
  }

  @Override
  public Object getHashCodeForState(AbstractState pElementKey, Precision pPrecisionKey) {
    UsageState funElement = (UsageState) pElementKey;
    return wrappedReducer.getHashCodeForState(funElement.getWrappedState(), pPrecisionKey);
  }

  @Override
  public Precision getVariableReducedPrecision(Precision pPrecision, Block pContext) {
    return wrappedReducer.getVariableReducedPrecision(pPrecision, pContext);
  }

  @Override
  public Precision getVariableExpandedPrecision(
      Precision rootPrecision, Block rootContext, Precision reducedPrecision) {
    return wrappedReducer.getVariableExpandedPrecision(
        rootPrecision, rootContext, reducedPrecision);
  }

  @Override
  public int measurePrecisionDifference(Precision pPrecision, Precision pOtherPrecision) {
    return wrappedReducer.measurePrecisionDifference(pPrecision, pOtherPrecision);
  }

  @Override
  public AbstractState rebuildStateAfterFunctionCall(
      AbstractState pRootState,
      AbstractState pEntryState,
      AbstractState pExpandedState,
      FunctionExitNode pExitLocation) {
    return pExpandedState;
  }
}
