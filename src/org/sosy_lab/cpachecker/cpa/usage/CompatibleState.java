// This file is part of CPAchecker,
// a tool for configurable software verification:
// https://cpachecker.sosy-lab.org
//
// SPDX-FileCopyrightText: 2007-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.cpa.usage;

import org.sosy_lab.cpachecker.core.interfaces.AbstractState;
import org.sosy_lab.cpachecker.util.identifiers.AbstractIdentifier;

public interface CompatibleState extends Comparable<CompatibleState>, AbstractState {

  default boolean isCompatibleWith(@SuppressWarnings("unused") CompatibleState state) {
    return true;
  }

  public CompatibleNode getCompatibleNode();

  default boolean isRelevantFor(@SuppressWarnings("unused") AbstractIdentifier pIdent) {
    return true;
  }
}
