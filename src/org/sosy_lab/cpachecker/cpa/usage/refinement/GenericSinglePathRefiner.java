// This file is part of CPAchecker,
// a tool for configurable software verification:
// https://cpachecker.sosy-lab.org
//
// SPDX-FileCopyrightText: 2007-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.cpa.usage.refinement;

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.errorprone.annotations.ForOverride;
import org.sosy_lab.cpachecker.core.interfaces.AdjustablePrecision;
import org.sosy_lab.cpachecker.exceptions.CPAException;
import org.sosy_lab.cpachecker.util.Pair;
import org.sosy_lab.cpachecker.util.statistics.StatCounter;
import org.sosy_lab.cpachecker.util.statistics.StatTimer;
import org.sosy_lab.cpachecker.util.statistics.StatisticsWriter;

public abstract class GenericSinglePathRefiner
    extends WrappedConfigurableRefinementBlock<
        Pair<ExtendedARGPath, ExtendedARGPath>, Pair<ExtendedARGPath, ExtendedARGPath>> {

  private StatTimer totalTimer = new StatTimer("Time for generic refiner");
  private StatCounter numberOfRefinements = new StatCounter("Number of refinements");
  private StatCounter numberOfRepeatedPath = new StatCounter("Number of repeated paths");

  private final boolean continueAfterFalse;

  protected GenericSinglePathRefiner(
      ConfigurableRefinementBlock<Pair<ExtendedARGPath, ExtendedARGPath>> pWrapper,
      boolean pContinue) {
    super(pWrapper);
    continueAfterFalse = pContinue;
  }

  @Override
  public final RefinementResult performBlockRefinement(
      Pair<ExtendedARGPath, ExtendedARGPath> pInput) throws CPAException, InterruptedException {
    totalTimer.start();

    try {
      ExtendedARGPath firstPath = pInput.getFirst();
      ExtendedARGPath secondPath = pInput.getSecond();

      // Refine paths separately
      RefinementResult result1 = refinePath(firstPath);
      if (result1.isFalse() && !continueAfterFalse) {
        return result1;
      }
      RefinementResult result2 = refinePath(secondPath);
      if (!result1.isFalse() && !result2.isFalse()) {
        return wrappedRefiner.performBlockRefinement(pInput);
      }
      Iterable<AdjustablePrecision> completePrecisions =
          Iterables.concat(result1.getPrecisions(), result2.getPrecisions());
      if (!result1.isFalse()) {
        assert result2.isFalse();
        result1 = result2;
      }

      // Do not need add any precision if the result is confirmed
      result1.resetPrecision(completePrecisions);
      return result1;
    } finally {
      totalTimer.stop();
    }
  }

  private RefinementResult refinePath(ExtendedARGPath path)
      throws CPAException, InterruptedException {
    Preconditions.checkArgument(!path.isUnreachable(), "Path could not be unreachable here");

    if (path.isRefinedAsReachableBy(this)) {
      // Means that is is reachable, but other refiners declined it.
      // Now the pair changes. Do not refine it again.
      numberOfRepeatedPath.inc();
      return RefinementResult.createTrue();
    }

    numberOfRefinements.inc();
    RefinementResult result = call(path);
    if (result.isTrue()) {
      path.setAsTrueBy(this);
      return result;
    } else {
      path.setAsFalse();
      return result;
    }
  }

  @Override
  public final void printStatistics(StatisticsWriter pOut) {
    StatisticsWriter writer =
        pOut.spacer().put(totalTimer).put(numberOfRefinements).put(numberOfRepeatedPath);

    printAdditionalStatistics(writer);
    wrappedRefiner.printStatistics(writer);
  }

  @ForOverride
  protected void printAdditionalStatistics(@SuppressWarnings("unused") StatisticsWriter pOut) {}

  protected abstract RefinementResult call(ExtendedARGPath path)
      throws CPAException, InterruptedException;
}
