/*
 *  CPAchecker is a tool for configurable software verification.
 *  This file is part of CPAchecker.
 *
 *  Copyright (C) 2007-2019  Dirk Beyer
 *  All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.sosy_lab.cpachecker.cpa.usage.refinement;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.FluentIterable.from;
import static org.sosy_lab.common.collect.Collections3.transformedImmutableListCopy;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import org.sosy_lab.cpachecker.core.interfaces.AbstractState;
import org.sosy_lab.cpachecker.cpa.arg.ARGState;
import org.sosy_lab.cpachecker.cpa.arg.path.ARGPath;

public class ARGPathRestorator implements PathRestorator {

  private final Function<ARGState, Integer> getStateId;

  public ARGPathRestorator(Function<ARGState, Integer> idExtractor) {
    getStateId = idExtractor;
  }

  @Override
  public ARGPath computePath(ARGState pLastElement, List<AbstractState> pStack) {
    // Temporary implementation
    return computePath(pLastElement, ImmutableSet.of(), pStack);
  }

  @Override
  public ARGPath computePath(
      ARGState pLastElement, Set<List<Integer>> pRefinedStates, List<AbstractState> pStack) {

    List<ARGState> states = new ArrayList<>(); // reversed order
    Set<ARGState> seenElements = new HashSet<>();

    // each element of the path consists of the abstract state and the outgoing
    // edge to its successor

    ARGState currentARGState = pLastElement;
    states.add(currentARGState);
    seenElements.add(currentARGState);
    Deque<ARGState> backTrackPoints = new ArrayDeque<>();
    Deque<List<ARGState>> backTrackOptions = new ArrayDeque<>();

    while (!currentARGState.getParents().isEmpty()) {
      Iterator<ARGState> parents = currentARGState.getParents().iterator();

      ARGState parentElement = parents.next();
      while (seenElements.contains(parentElement) && parents.hasNext()) {
        // while seenElements already contained parentElement, try next parent
        parentElement = parents.next();
      }

      if (seenElements.contains(parentElement)) {
        // Backtrack
        checkArgument(
            !backTrackPoints.isEmpty(), "No ARG path from the target state to a root state.");
        ARGState backTrackPoint = backTrackPoints.pop();
        ListIterator<ARGState> stateIterator = states.listIterator(states.size());
        while (stateIterator.hasPrevious() && !stateIterator.previous().equals(backTrackPoint)) {
          stateIterator.remove();
        }
        List<ARGState> options = backTrackOptions.pop();
        for (ARGState parent : backTrackPoint.getParents()) {
          if (!options.contains(parent)) {
            seenElements.add(parent);
          }
        }
        currentARGState = backTrackPoint;
      } else {
        // Record backtracking options
        if (parents.hasNext()) {
          List<ARGState> options = new ArrayList<>(1);
          while (parents.hasNext()) {
            ARGState parent = parents.next();
            if (!seenElements.contains(parent)) {
              options.add(parent);
            }
          }
          if (!options.isEmpty()) {
            backTrackPoints.push(currentARGState);
            backTrackOptions.push(options);
          }
        }

        seenElements.add(parentElement);
        states.add(parentElement);

        currentARGState = parentElement;
      }
    }

    List<ARGState> pathStates = Lists.reverse(states);
    List<Integer> ids = transformedImmutableListCopy(pathStates, getStateId);
    if (from(pRefinedStates).anyMatch(ids::containsAll)) {
      return null;
    }

    return new ARGPath(Lists.reverse(states));
  }

  @Override
  public ARGPathIterator iterator(ARGState pTarget, List<AbstractState> pStack) {
    return new DummyPathIterator(pTarget, pStack);
  }

  class DummyPathIterator implements ARGPathIterator {
    final ARGState target;
    boolean computeOnePath;
    List<AbstractState> stack;

    DummyPathIterator(ARGState pTarget, List<AbstractState> pStack) {
      target = pTarget;
      computeOnePath = false;
      stack = pStack;
    }

    @Override
    public ARGPath nextPath(Set<List<Integer>> pRefinedStatesIds) {
      if (computeOnePath) {
        return null;
      } else {
        computeOnePath = true;
        return ARGPathRestorator.this.computePath(target, pRefinedStatesIds, stack);
      }
    }
  }
}
