// This file is part of CPAchecker,
// a tool for configurable software verification:
// https://cpachecker.sosy-lab.org
//
// SPDX-FileCopyrightText: 2007-2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.cpa.usage.storage;

import org.sosy_lab.common.configuration.Configuration;
import org.sosy_lab.common.configuration.InvalidConfigurationException;
import org.sosy_lab.common.configuration.Option;
import org.sosy_lab.common.configuration.Options;
import org.sosy_lab.cpachecker.cpa.usage.storage.UnsafeDetector.UnsafeMode;

@Options(prefix = "cpa.usage")
public final class UsageConfiguration {

  @Option(description = "output FALSE only if there are true unsafes", secure = true)
  private boolean considerOnlyTrueUnsafes = false;

  @Option(
      name = "unsafedetector.ignoreEmptyLockset",
      description = "ignore unsafes only with empty callstacks",
      secure = true)
  private boolean ignoreEmptyLockset = true;

  @Option(name = "unsafedetector.unsafeMode", description = "defines what is unsafe", secure = true)
  private UnsafeMode unsafeMode = UnsafeMode.RACE;

  @Option(
      name = "unsafedetector.intLock",
      description = "A name of interrupt lock for checking deadlock free",
      secure = true)
  private String intLockName = null;

  @Option(
      name = "processCoveredUsages",
      description = "Should we process the same blocks with covered sets of locks",
      secure = true)
  private boolean processCoveredUsages = true;

  @Option(
      name = "useConcurrentExtraction",
      description = "Use several threads for usage extraction",
      secure = true)
  private boolean useConcurrentExtraction = true;

  @Option(
      name = "isRevertedPriority",
      description = "Reverted priority: zero is the highest",
      secure = true)
  private boolean isRevertedPriority = false;

  public UsageConfiguration(Configuration config) throws InvalidConfigurationException {
    config.inject(this);
  }

  public boolean considerOnlyTrueUnsafes() {
    return considerOnlyTrueUnsafes;
  }

  boolean ignoreEmptyLockset() {
    return ignoreEmptyLockset;
  }

  public UnsafeMode getUnsafeMode() {
    return unsafeMode;
  }

  String getIntLockName() {
    return intLockName;
  }

  public boolean getProcessCoveredUsages() {
    return processCoveredUsages;
  }

  public boolean useConcurrentExtraction() {
    return useConcurrentExtraction;
  }

  public boolean isRevertedPriority() {
    return isRevertedPriority;
  }
}
