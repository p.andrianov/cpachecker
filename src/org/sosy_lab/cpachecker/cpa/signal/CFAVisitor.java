// This file is part of CPAchecker,
// a tool for configurable software verification:
// https://cpachecker.sosy-lab.org
//
// SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.cpa.signal;

import org.sosy_lab.cpachecker.cfa.model.c.CDeclarationEdge;
import org.sosy_lab.cpachecker.cfa.model.c.CFunctionCallEdge;
import org.sosy_lab.cpachecker.cfa.model.c.CStatementEdge;

public interface CFAVisitor {
  public SignalEffect visitDeclarationEdge(CDeclarationEdge declEdge);

  public SignalEffect visitFunctionCallEdge(CFunctionCallEdge fcallEdge);

  public SignalEffect visitStatementEdge(CStatementEdge stmntEdge);
}
