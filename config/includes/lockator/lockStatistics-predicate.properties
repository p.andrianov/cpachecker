# This file is part of CPAchecker,
# a tool for configurable software verification:
# https://cpachecker.sosy-lab.org
#
# SPDX-FileCopyrightText: 2007-2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# The configuration is used as a precise configuration for lock analysis,

#include lockStatistics-core.properties

CompositeCPA.cpas = cpa.location.LocationCPA, cpa.callstack.CallstackCPA, cpa.thread.ThreadCPA, cpa.lock.LockCPA, cpa.predicate.PredicateCPA, cpa.functionpointer.FunctionPointerCPA
cpa.usage.refinement.refinementChain = IdentifierIterator, PointIterator, UsageIterator, PathIterator, PredicateRefiner

solver.solver = SMTINTERPOL
analysis.algorithm.CEGAR = true

cpa.usage.considerOnlyTrueUnsafes = true
cpa.usage.refinement.iterationLimit = 2
cpa.usage.refinement.disableAllCaching = true
cpa.lock.refinement = false

[cegar]
refiner = cpa.usage.refinement.IdentifierIterator
globalRefinement = true
# maxIterations = 20

[cpa.predicate]
useBitwiseAxioms = true
encodeBitvectorAs = INTEGER
encodeFloatAs = RATIONAL
useArraysForHeap = false
defaultArrayLength = 1
maxArrayLength = 1
nondetFunctions = ldv_undef_int
useHavocAbstraction = false
useConstraintOptimization = false
# New heuristics: type detection and void* tracking
revealAllocationTypeFromLhs = true
deferUntypedAllocations = true
# New heuristic for cillified files: pre-populate small structures with all their fields
# maxPreFilledAllocationSize = 0
# An option enabling PathFormulaWithUF, CToFormulaWithUFConverter and appropriate merging policy
handlePointerAliasing =  true
# Setting the option to make memory allocations always return correct addresses
memoryAllocationsAlwaysSucceed = true
# blk.alwaysAtJoin     = true
blk.alwaysAfterAssume = true
trackFunctionPointers=false
refinement.checkPath = false
allowedUnsupportedFunctions=memset,memmove,memcpy,__atomic_store_n,__builtin_umull_overflow, __builtin_mul_overflow, __builtin_add_overflow
