<?xml version="1.0" ?>

<!-- This file is part of CPAchecker, -->
<!-- a tool for configurable software verification: -->
<!-- https://cpachecker.sosy-lab.org -->
<!-- -->
<!-- SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org> -->
<!-- -->
<!-- SPDX-License-Identifier: Apache-2.0 -->

<!--
CPAchecker crashes on these input files (that means, these programs are failing tests).
These programs are used to test CFA mutator when it tries to make minimal failing test.
-->

<!DOCTYPE benchmark PUBLIC "+//IDN sosy-lab.org//DTD BenchExec benchmark 2.3//EN" "https://www.sosy-lab.org/benchexec/benchmark-2.3.dtd">
<benchmark walltimelimit="1100s" tool="cpachecker">
    <option name="-setprop">cfaMutation=true</option>
    <option name="-setprop">limits.time.wall=1000s</option>
    <option name="-setprop">limits.time.cpu=-1ns</option>

	<option name="-setprop">output.disable=false</option>
    <option name="-setprop">cfa.export=true</option>
    <option name="-setprop">cfa.exportPerFunction=true</option>
    <option name="-setprop">cfa.exportToC=true</option>
    <option name="-setprop">cfa.serialize=true</option>

	<columns>
		<column title="m.rounds total">total count of mutation rounds</column>
		<column title="m.rounds w/same error">successful mutations (sought-for error)</column>
		<column title="m.rounds w/no error">unsuccessfull mutations (no error)</column>
		<column title="m.rounds w/other problem">unsuccessfull mutations (other problems)</column>

		<column title="local nodes before">count of nodes in function CFAs before mutations</column>
		<column title="local nodes after">count of nodes in function CFAs after mutations</column>
		<column title="local edges before">count of edges in function CFAs before mutations</column>
		<column title="local edges after">count of edges in function CFAs after mutations</column>
		<column title="global decls before">count of global declarations before mutations</column>
		<column title="global decls after">count of global declarations after mutations</column>
		<column title="local functions before">count of functions in function CFAs before mutations</column>
		<column title="local functions after">count of functions in function CFAs after mutations</column>
		<column title="CFAs CC sum before">cyclomatic complexity of function CFAs before mutations</column>
		<column title="CFAs CC sum after">cyclomatic complexity of function CFAs after mutations</column>

		<column title="full nodes before">count of nodes in full CFA before mutations</column>
		<column title="full nodes after">count of nodes in full CFA after mutations</column>
		<column title="full edges before">count of edges in full CFA before mutations</column>
		<column title="full edges after">count of edges in full CFA after mutations</column>
		<column title="full functions before">count of functions in full CFA before mutations</column>
		<column title="full functions after">count of functions in full CFA after mutations</column>
		<column title="CC of full CFA before">cyclomatic complexity of full CFA before mutations</column>
		<column title="CC of full CFA after">cyclomatic complexity of full CFA after mutations</column>

		<column title="original time">time for original analysis run</column>
		<column title="time after mutations">time for analysis after mutations</column>
		<column title="time after rollbacks">time for analysis after rollback</column>
	</columns>

	<rundefinition name="IMC">
		<option name="-setprop">cpa.predicate.memoryAllocationsAlwaysSucceed=true</option>
		<option name="-bmc-interpolation"/>
		<option name="-setprop">imc.fallBack=false</option>
		<option name="-setprop">cfa.transformIntoSingleLoop=true</option>
	</rundefinition>

	<rundefinition name="Predicate-Abstraction">
		<option name="-setprop">cpa.predicate.memoryAllocationsAlwaysSucceed=true</option>
		<option name="-predicateAnalysis"/>
	</rundefinition>

	<rundefinition name="Impact">
		<option name="-setprop">cpa.predicate.memoryAllocationsAlwaysSucceed=true</option>
		<option name="-predicateAnalysis-ImpactRefiner-ABEl"/>
	</rundefinition>

	<tasks name="issue-992">
		<include>../../programs/benchmarks/array-industry-pattern/array_shadowinit.i</include>
		<include>../../programs/benchmarks/array-tiling/mbpr2.c</include>
		<include>../../programs/benchmarks/array-multidimensional/init-non-constant-2-n-u.c</include>
		<include>../../programs/benchmarks/array-patterns/array1_pattern.c</include>
		<include>../../programs/benchmarks/array-cav19/array_init_var_plus_ind.c</include>
		<include>../../programs/benchmarks/array-fpi/eqn1.c</include>
		<include>../../programs/benchmarks/openssl/s3_clnt.blast.01.i.cil-1.c</include>
		<include>../../programs/benchmarks/floats-cbmc-regression/float18.i</include>
		<include>../../programs/benchmarks/float-benchs/Muller_Kahan.c</include>
		<include>../../programs/benchmarks/float-benchs/Muller_Kahan.c.p+cfa-reducer.c</include>
		<include>../../programs/benchmarks/float-benchs/sqrt_Householder_constant.c.p+cfa-reducer.c</include>
		<include>../../programs/benchmarks/floats-esbmc-regression/digits_bad_for.c</include>
		<include>../../programs/benchmarks/loop-floats-scientific-comp/loop1-1.c</include>
		<include>../../programs/benchmarks/list-properties/alternating_list-1.i</include>
		<include>../../programs/benchmarks/forester-heap/dll-rb-cnstr_1-1.i</include>
		<include>../../programs/benchmarks/loops/eureka_01-1.c</include>
		<include>../../programs/benchmarks/nla-digbench-scaling/freire1_valuebound10.c</include>
		<propertyfile>../../programs/benchmarks/properties/unreach-call.prp</propertyfile>
	</tasks>

</benchmark>
