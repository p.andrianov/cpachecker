<?xml version="1.0" ?>

<!-- This file is part of CPAchecker, -->
<!-- a tool for configurable software verification: -->
<!-- https://cpachecker.sosy-lab.org -->
<!-- -->
<!-- SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org> -->
<!-- -->
<!-- SPDX-License-Identifier: Apache-2.0 -->

<!--
CPAchecker crashes on these input files (that means, these programs are failing tests).
These programs are used to test CFA mutator when it tries to make minimal failing test.
-->

<!DOCTYPE benchmark PUBLIC "+//IDN sosy-lab.org//DTD BenchExec benchmark 2.3//EN" "https://www.sosy-lab.org/benchexec/benchmark-2.3.dtd">
<benchmark tool="cpachecker" walltimelimit="1100s">

    <option name="-setprop">cpa.callstack.unsupportedFunctions=__VERIFIER_nonexisting_dummy_function</option>
    <option name="-setprop">cpa.predicate.allowedUnsupportedFunctions=memset,memcpy,__builtin_add_overflow,__builtin_mul_overflow,__builtin_va_arg</option>
    <option name="-setprop">cpa.value.allowedUnsupportedFunctions=memset,memcpy,__builtin_add_overflow,__builtin_mul_overflow,__builtin_va_arg</option>
    <option name="-setprop">counterexample.export.extendedWitnessFile=witness.%d.graphml</option>
    <option name="-setprop">counterexample.export.exportExtendedWitness=true</option>
    <option name="-setprop">counterexample.export.compressWitness=false</option>
    <option name="-setprop">cpa.arg.witness.removeInsufficientEdges=false</option>
    <option name="-setprop">counterexample.export.exportCounterexampleCoverage=true</option>
    <option name="-setprop">counterexample.export.prefixAdditionalCoverageFile=Counterexample.%d.additionalCoverage.info</option>
    <option name="-setprop">additionalCoverage.file=additionalCoverage.info</option>
    <option name="-setprop">shutdown.timeout=100</option>
    <option name="-heap">4000m</option>
    <option name="-64"/>
    <option name="-smg-ldv"/>
    <option name="-setprop">CompositeCPA.cpas=cpa.location.LocationCPA,cpa.callstack.CallstackCPA,cpa.smg.SMGCPA</option>
    <option name="-setprop">parser.readLineDirectives=false</option>
    <option name="-setprop">cpa.smg.memcpyFunctions=__VERIFIER_memcpy</option>
    <option name="-setprop">cpa.smg.memsetFunctions=__VERIFIER_memset</option>

    <option name="-setprop">cfaMutation=true</option>
    <option name="-setprop">limits.time.wall=1000s</option>
    <option name="-setprop">limits.time.cpu=-1ns</option>

	<option name="-setprop">output.disable=false</option>
    <option name="-setprop">cfa.export=true</option>
    <option name="-setprop">cfa.exportPerFunction=true</option>
    <option name="-setprop">cfa.exportToC=true</option>
    <option name="-setprop">cfa.serialize=true</option>

	<columns>
		<column title="total rounds">total count of mutation rounds</column>
		<column title="rounds with same error">successful mutations (sought-for error)</column>
		<column title="rounds with no error">unsuccessfull mutations (no error)</column>
		<column title="rounds with other problem">unsuccessfull mutations (other problems)</column>

		<column title="local nodes before">count of nodes in function CFAs before mutations</column>
		<column title="local nodes after">count of nodes in function CFAs after mutations</column>
		<column title="local edges before">count of edges in function CFAs before mutations</column>
		<column title="local edges after">count of edges in function CFAs after mutations</column>
		<column title="global decls before">count of global declarations before mutations</column>
		<column title="global decls after">count of global declarations after mutations</column>
		<column title="local functions before">count of functions in function CFAs before mutations</column>
		<column title="local functions after">count of functions in function CFAs after mutations</column>
		<column title="CFAs CC sum before">cyclomatic complexity of function CFAs before mutations</column>
		<column title="CFAs CC sum after">cyclomatic complexity of function CFAs after mutations</column>

		<column title="full nodes before">count of nodes in full CFA before mutations</column>
		<column title="full nodes after">count of nodes in full CFA after mutations</column>
		<column title="full edges before">count of edges in full CFA before mutations</column>
		<column title="full edges after">count of edges in full CFA after mutations</column>
		<column title="full functions before">count of functions in full CFA before mutations</column>
		<column title="full functions after">count of functions in full CFA after mutations</column>
		<column title="CC of full CFA before">cyclomatic complexity of full CFA before mutations</column>
		<column title="CC of full CFA after">cyclomatic complexity of full CFA after mutations</column>

		<column title="original time">time for original analysis run</column>
		<column title="time after mutations">time for analysis after mutations</column>
		<column title="time after rollbacks">time for analysis after rollback</column>
	</columns>

    <rundefinition name="Function-AE-Chain-Statement-Expression-Declaration-Global"/>

	<rundefinition name="Expression-Declaration-Global">
        <option name="-setprop">cfaMutation.strategies=ExpressionRemover,DeclarationEdgeRemover,GlobalDeclarationRemover</option>
	</rundefinition>
	<rundefinition name="AE-Chain-Statement-Expression-Declaration-Global">
        <option name="-setprop">cfaMutation.strategies=AssumeEdgesRemover,StatementChainRemover,StatementEdgeRemover,ExpressionRemover,DeclarationEdgeRemover,GlobalDeclarationRemover</option>
	</rundefinition>
	<rundefinition name="Function">
        <option name="-setprop">cfaMutation.strategies=FunctionBodyRemover</option>
	</rundefinition>
	<rundefinition name="Chain">
        <option name="-setprop">cfaMutation.strategies=StatementChainRemover</option>
	</rundefinition>

    <tasks>
        <include>../../programs/issues-for-mutation/drivers|hwmon|asus-ec-sensors.ko.i</include>
        <include>../../programs/issues-for-mutation/drivers|media|i2c|adp1653.ko.i</include>
        <include>../../programs/issues-for-mutation/drivers|mtd|nand|raw|oxnas_nand.ko.i</include>
        <include>../../programs/issues-for-mutation/drivers|platform|x86|amd_hsmp.ko.i</include>
        <include>../../programs/issues-for-mutation/drivers|platform|x86|sony-laptop.ko.i</include>
        <!-- timeout <include>../../programs/issues-for-mutation/drivers|video|fbdev|udlfb.ko.i</include> -->
    </tasks>
    <propertyfile>../../programs/benchmarks/properties/valid-memsafety.prp</propertyfile>
</benchmark>
